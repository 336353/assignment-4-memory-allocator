#ifndef MEMORY_ALLOCATOR_TEST_H
#define MEMORY_ALLOCATOR_TEST_H

#include <stdio.h>

#include "mem.h"
#include "mem_internals.h"

int first_test();
int second_test();

#endif //MEMORY_ALLOCATOR_TEST_H
